import os

static_path = os.path.join(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)), 'static')
template_path = os.path.join(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)), 'template')  
    
setting_dic = {
    'static_path': static_path,
    'template_path': template_path
}