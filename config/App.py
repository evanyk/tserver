from tornado.web import Application
from config.HandlerConfig import *
from config.SettingConfig import *

class App(Application):
    def __init__(self):
        Application.__init__(self, handler_map, **setting_dic)