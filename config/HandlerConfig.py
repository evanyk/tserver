from handler import *

handler_map = [
        (r"/", TestHandler),
        (r"/sock", TestSocketHandler),
        (r"/res/(.*)", TestResponseHandler),
        (r"/getp", TestPostFetchHandler),
        (r"/post", TestPostDealHandler),
        (r"/ws", SocketHandler)
    ]