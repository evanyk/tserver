from tornado.websocket import WebSocketHandler
from datetime import datetime

class SocketHandler(WebSocketHandler):
    def initialize(self):
        pass

    def open(self):
        print("connected")

    def on_message(self, msg):
        time = str(datetime.now())
        self.write_message(u"[" + time + "]Re: " + msg)

    def on_close(self):
        print("disconnected")