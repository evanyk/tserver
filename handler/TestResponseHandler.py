from tornado.web import RequestHandler
import json

class TestResponseHandler(RequestHandler):
    def get(self, key):
        print('key: ' + key)
        print('k: ' + self.get_argument('k'))
        tem = {
            'test': 'json'
        }
        
        self.write(json.JSONEncoder().encode(tem))