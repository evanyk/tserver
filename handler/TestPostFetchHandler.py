from tornado.web import RequestHandler
from tornado.httpclient import AsyncHTTPClient
import json


class TestPostFetchHandler(RequestHandler):
    def initialize(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')


    async def get(self):
        client = AsyncHTTPClient()
        d = json.JSONEncoder().encode({'a':1,'b':2})
        res = await client.fetch('http://localhost:8000/post',method='POST', body=d)
        self.write(res.body)
