from tornado.web import RequestHandler
import json

class TestPostDealHandler(RequestHandler):
    def post(self):
        tmp = json.loads(self.request.body)
        tmp['c'] = 3
        self.write(json.JSONEncoder().encode(tmp))