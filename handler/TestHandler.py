from tornado.web import RequestHandler
from tornado.httpclient import AsyncHTTPClient
import json

class TestHandler(RequestHandler):
    def initialize(self):
        # db 
        pass

    async def get(self):
        client = AsyncHTTPClient()
        res = await client.fetch('http://localhost:8000/res/key?k=1')
        tm = json.loads(res.body)
        client.close()
        self.write(tm['test']);
