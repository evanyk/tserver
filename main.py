from tornado.ioloop import IOLoop
from tornado.httpserver import HTTPServer
from config import App

if __name__ == "__main__":
    app = App()
    server = HTTPServer(app)
    server.listen(8000)
    IOLoop.instance().start()